<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\TestMail;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function sendMail($value='')
    {
        # code...
        $arrayName = array('name' => 'Haridas Shinde');
        Mail::to('harishinde2@gmail.com')
                ->send(new TestMail($arrayName));
        echo "success send mail";
    }
}
