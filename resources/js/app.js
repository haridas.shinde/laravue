/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
import moment from 'moment';
// import { Form, HasError, AlertError } from 'vform';
// Vue.component(HasError.name, HasError)
// Vue.component(AlertError.name, AlertError)
// window.Form = Form;

// gate authorization
import Gate from "./Gate";
const $gate = new Gate(window.user);
// Vue.prototype.$gate = $gate;

// progressbar
// import VueProgressBar from 'vue-progressbar';
// Vue.use(VueProgressBar, {
//     color: 'rgb(143, 255, 199)',
//     failedColor: 'red',
//     height: '15px'
// })

// window.Fire = new createApp();

// end progressbar


import Swal from 'sweetalert2';
window.Swal = Swal;
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.toast = Toast;

// end sweetalert

// router
// import VueRouter from 'vue-router'
// Vue.use(VueRouter)

const App = {
    setup() {
        return { search: '' }
    },
    methods: {

        // searchit: _.debounce(() => {
        //     console.log('test')
        //     // this.$emit("enlargeText", "someValue");

        // }, 10),

        printme() {
            window.print();
        }
    }
}

import { createApp } from "vue";
import { createRouter, createWebHashHistory } from 'vue-router'
const app = Vue.createApp(App);


let routes = [
    { path: '/', component: () => import('./components/DashboardComponent.vue') },
    { path: '/dashboard', component: () => import('./components/DashboardComponent.vue') },
    { path: '/not-found', component: () => import('./components/NotFound.vue') },
    // { path: '/developer', component: require('./components/DeveloperComponent.vue').default },
    { path: '/profile', component: () => import('./components/ProfileComponent.vue') },
    // { path: '/users', component: require('./components/UserComponent.vue').default },
    // { path: '*', component: require('./components/NotFound.vue').default },

]
// const router = new VueRouter({
//     mode: 'history',
//     routes // short for `routes: routes`
// })

const router = createRouter({
    history: createWebHashHistory(),
    routes // short for `routes: routes`

})

// end router
// Vue.filter('upText', function (text) {
//     return text.charAt(0).toUpperCase() + text.slice(1)
// });
// Vue.filter('myDate', function (created) {
//     return moment(created).format('MMMM Do YYYY');
// });
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
// Vue.component('pagination', require('laravel-vue-pagination'));
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Vue.component('not-found',require('./components/NotFound.vue').default);
// Vue.component('passport-clients',require('./components/passport/Clients.vue').default);
// Vue.component('passport-authorized-clients',require('./components/passport/AuthorizedClients.vue').default);
// Vue.component('passport-personal-access-tokens',require('./components/passport/PersonalAccessTokens.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
//     router,
//     data: {
//         search: ''
//     },
//     methods: {
//         searchit: _.debounce(() => {
//             Fire.$emit('searching');
//         }, 1000),

//         printme() {
//             window.print();
//         }
//     }
// });
app.use(router)
app.mount("#app")
