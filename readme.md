<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## Laravel Vue Installation

It's just like any other Laravel project. Basically here is how you use it for yourself.

- Clone the repo : `git clone git@gitlab.com:haridas.shinde/jewelleryapp.git`
- cd to `project folder`.
- Run `composer install`
- Run  `npm install`
- Run `php artisan passport:install`
- Save as the `.env.example` to `.env` and set your database information
- Run `php artisan key:generate` to generate the app key
- Run `php artisan migrate`
- Run `php artisan serve`
- Done !!! Enjoy Customizing and building awesome app
